import { Row, Col } from "antd";
import { withTranslation } from "react-i18next";
import { SvgIcon } from "../../common/SvgIcon";
import Container from "../../common/Container";

import i18n from "i18next";
import {
  FooterSection,
  Title,
  NavLink,
  Extra,
  LogoContainer,
  Para,
  Large,
  Chat,
  Empty,
  FooterContainer,
  Language,
  Label,
  LanguageSwitch,
  LanguageSwitchContainer,
} from "./styles";
import MediaQuery from 'react-responsive'
         
         
interface SocialLinkProps {
  href: string;
  src: string;
}

const Footer = ({ t }: any) => {
  const handleChange = (language: string) => {
    i18n.changeLanguage(language);
  };

  const SocialLink = ({ href, src }: SocialLinkProps) => {
    return (
      <a
        href={href}
        target="_blank"
        rel="noopener noreferrer"
        key={src}
        aria-label={src}
      >
        <SvgIcon src={src} width="25px" height="25px" />
      </a>
    );
  };

  return (
    <>
      <FooterSection >
        <Container>
          <Row justify="space-between">
          <MediaQuery maxWidth={1224}>
          <NavLink to="/">
              <LogoContainer>
                <SvgIcon
                  src="logo2.svg"
                  aria-label="homepage"
                  width="200px"
                  height="100px"
                />
              </LogoContainer>
            </NavLink>
          </MediaQuery>
          <MediaQuery minWidth={600}>
          <NavLink to="/">
              <LogoContainer>
                <SvgIcon
                  src="logo.svg"
                  aria-label="homepage"
                  width="200px"
                  height="100px"
                />
              </LogoContainer>
            </NavLink>
          </MediaQuery>
            <br/>
           {/**
            * <Col lg={10} md={10} sm={12} xs={12}>
              <Language>{t("Contact")}</Language>
              <Large to="/">{t("Tell us everything")}</Large>
              <Para>
                {t(`Do you have any question? Feel free to reach out.`)}
              </Para>
              <a href="mailto:l.qqbadze@gmail.com">
                <Chat>{t(`Let's Chat`)}</Chat>
              </a>
            </Col>
            */} 
           
            <Col lg={6} md={6} sm={12} xs={12}>
              <Large left="true" to="/">
                {/*t("Support Center")*/}
              </Large>
              <Large left="true" to="/">
                {/*t("Customer Support")*/}
              </Large>
            </Col>
          </Row>
          <Row justify="space-between">
            <Col lg={6} md={6} sm={12} xs={12}>
              <Language>{t("Address")}</Language>
              <Para>Lafayette </Para>
              <Para>2131 Palastine Street , Tunis</Para>
            
            </Col>
            <Col lg={6} md={6} sm={12} xs={12}>
              <Language>{t("Qui sommes nous ?")}</Language>
              <Para>Contacter nous</Para>
              <Para>Terms & condition d'utilisation</Para>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12}>
              <Empty/><Empty/>
              <Title>{t("Réseau Sociaux")}</Title>
              <Large left="true" to="/">
              <SocialLink
                href="https://www.linkedin.com/in/lasha-kakabadze/"
                src="facebook.svg"
              />{t("    Facebook")}
              </Large>
              <Large left="true" to="/">
              
              <SocialLink
                href="https://www.linkedin.com/in/lasha-kakabadze/"
                src="instagram.svg"
              />{t("    Instagram")}
              </Large>
             
            </Col>
            <Col lg={6} md={6} sm={12} xs={12}>
            <Empty/><Empty/>
            <Title>{t("Contact")}</Title>
              <Large left="true" to="/">
              <SocialLink
                href="https://www.linkedin.com/in/lasha-kakabadze/"
                src="e-mail.svg"
              />{t("Mail : info@westic.com")}
              </Large>
              <Large left="true" to="/">
              
              <SocialLink
                href="https://www.linkedin.com/in/lasha-kakabadze/"
                src="phone-call.svg"
              />{t("Téléphone : +216889911")}
              </Large>
            </Col>
           
          </Row>
        </Container>    
      </FooterSection>
    </>
  );
};

export default withTranslation()(Footer);
