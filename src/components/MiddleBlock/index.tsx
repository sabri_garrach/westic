import { Row, Col } from "antd";
import { withTranslation } from "react-i18next";
import { Slide } from "react-awesome-reveal";
import { Button } from "../../common/Button";
import { MiddleBlockSection, Content, ContentWrapper } from "./styles";
import CSS from 'csstype';
import { SvgIcon } from "../../common/SvgIcon";
import MediaQuery from 'react-responsive'


interface MiddleBlockProps {
  title: string;
  content: string;
  button?: string;
  t: any;
  image:string;
  color:string;
  icon:string;
  images:any
  id?:string
}


const h1Styles: CSS.Properties = {
  marginTop:'7%'
};
const colorStyles: CSS.Properties = {
  display:'flex',
  marginTop:'7%',
   flexDirection: 'row',

};
const MiddleBlock = ({ title,id, content, button, t,icon,images,image }: MiddleBlockProps) => {
  const scrollTo = (id: string) => {
    const element = document.getElementById(id) as HTMLDivElement;
    element.scrollIntoView({
      behavior: "smooth",
    });
  };
  return (
    <MiddleBlockSection >
      <Slide triggerOnce>
        <Row justify="center" align="middle">
        <h6>{t(title)}</h6>
        <ContentWrapper> 
          <MediaQuery minWidth={1224}>
          <Row>
          <Col  span={8}>
                          <SvgIcon src={icon} width="100%" height="80%" />
                        </Col><Col  span={8}>
                          <SvgIcon src={'images2.svg'} width="100%" height="80%" />
                        </Col><Col  span={8}>
                          <SvgIcon src={'images3.svg'} width="100%" height="80%" />
                        </Col>
          </Row>   
          </MediaQuery>
   
          <MediaQuery maxWidth={1224}>
{typeof  images === "object" &&
                     images.map((item: any, id: number) => {
                      return (
 <SvgIcon src={item.image} width="100%" height="60%" /> 
                      );
                    })}  
          </MediaQuery>
         
          
                   
                  
                
          </ContentWrapper>

        </Row>
         
      </Slide>
    </MiddleBlockSection>
  );
};

export default withTranslation()(MiddleBlock);
