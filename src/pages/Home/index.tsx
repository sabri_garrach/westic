import { lazy } from "react";
import IntroContent from "../../content/IntroContent.json";
import MiddleBlockContent from "../../content/MiddleBlockContent.json";
import AboutContent from "../../content/AboutContent.json";
import MissionContent from "../../content/MissionContent.json";
import ProductContent from "../../content/ProductContent.json";
import ContactContent from "../../content/ContactContent.json";

const Contact = lazy(() => import("../../components/ContactForm"));
const MiddleBlock = lazy(() => import("../../components/MiddleBlock"));
const Container = lazy(() => import("../../common/Container"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
const ContentBlock = lazy(() => import("../../components/ContentBlock"));

const Home = () => {
  return (
    <Container >
      <ScrollToTop />
      <ContentBlock
        type="right"
        title={IntroContent.title}
        content={IntroContent.text}
        button={IntroContent.button}
        icon="developer.svg"
        id="intro"
      />
      <MiddleBlock
      images={AboutContent.subHeader}
      image=""
      color=""
      icon="image.svg"
        title={MiddleBlockContent.title}
        content={MiddleBlockContent.text}
        button={MiddleBlockContent.button}
      />
      <ContentBlock
        type="right"
        header=""
        title={AboutContent.title}
        content={AboutContent.text}
        section={AboutContent.section}
        icon="graphs.svg"
        id="about"
      />
      <ContentBlock
        type="left"
        header="Expediteur"
        title=""
        content=""
        section=""
        subHeader={AboutContent.subHeader}
        icon="notes.svg"
        id="about"
      /><ContentBlock
      type="left"
      header="Transporteur"
      title=""
      content=""
      section=""
      subHeader={AboutContent.subHeaderTransport}
      icon="medium.svg"
      id="about"
    />
    {/**
     * <ContentBlock
        type="right"
        header="hello header"
        title={MissionContent.title}
        content={MissionContent.text}
        icon="product-launch.svg"
        id="mission"
      />
      <ContentBlock
        type="left"
        title={ProductContent.title}
        content={ProductContent.text}
        icon="waving.svg"
        id="product"
      />
      <ContentBlock
        type="right"
        header=""
        title="3) Approuver la demande de Livraison"
        content="The following form demonstrates form validation in action. Contact form component reduces the amount of time it is being re-rendered by the user as it embraces uncontrolled form validation to reduce any unnecessary performance penalty"
        icon="approuve.svg"
        id="approuve"
      />
       <ContentBlock
        type="left"
        header=""
        title="4) Suivez vos colis"
        content="The following form demonstrates form validation in action. Contact form component reduces the amount of time it is being re-rendered by the user as it embraces uncontrolled form validation to reduce any unnecessary performance penalty"
        icon="suivie.svg"
        id="suivie"
      />
      <ContentBlock
        type="left"
        title="5) Payez et évaluer nos services"
        content="The following form demonstrates form validation in action. Contact form component reduces the amount of time it is being re-rendered by the user as it embraces uncontrolled form validation to reduce any unnecessary performance penalty"
        icon="pay.svg"
        id="suivie"
      />
     */}  
     <ContentBlock
        type="right"
        title="Comment ça marche ? "
        content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsu"
        icon="value2.svg"
        id="suivie"
      />
     {/**
      * <MiddleBlock
      image=""
      color=""
      icon="value2.svg"
        title="Comment ça marche ? "
        content={MiddleBlockContent.text}
        button={MiddleBlockContent.button}
       
      />
      */} 
      <Contact
        title={ContactContent.title}
        content={ContactContent.text}
        id="contact"
      />
      
    </Container>
  );
};

export default Home;
