import { lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import Footer from "../components/Footer";
import Header from "../components/Header";
import routes from "./config";
import { Styles } from "../styles/styles";
import CSS from 'csstype';

const h1Styles: CSS.Properties = {
  //backgroundImage=url(/img/svg/bg.svg),
  backgroundImage: "url(" + "/img/svg/footer.svg" + ")",
  backgroundPosition: 'center',
  //backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
};
const Router = () => {
  return (
    <Suspense fallback={null}>
         <Styles />
              
              
      <Header />
      <Switch >
        {routes.map((routeItem) => {
          return (
       
            <Route
              key={routeItem.component}
              path={routeItem.path}
              exact={routeItem.exact}
              component={lazy(() => import(`../pages/${routeItem.component}`))}
            />
          );
        })}
      </Switch>
      <Footer style={h1Styles} />
    </Suspense>
  );
};

export default Router;
