import { Row, Col } from "antd";
import { withTranslation } from "react-i18next";
import { SvgIcon } from "../../../common/SvgIcon";
import { ContentBlockProps } from "../types";
import { Fade } from "react-awesome-reveal";
import {
  LeftContentSection,
  Content,
  ContentWrapper,
  ServiceWrapper,
  MinTitle,
  MinPara,
} from "./styles";
import CSS from 'csstype';

const h1Styles: CSS.Properties = {
  backgroundImage: "url(" + "/img/svg/grisBG.svg" + ")",
  backgroundRepeat: 'no-repeat',
  backgroundPositionY:'-33px',
  backgroundPositionX:'-33px',
};
const LeftContentBlock = ( {
  
  icon,
  title,
  content,
  section,
  t,
  id,
  button,
  header,
  subHeader,

}: ContentBlockProps) => {
  const scrollTo = (id: string) => {
    const element = document.getElementById(id) as HTMLDivElement;
    element.scrollIntoView({
      behavior: "smooth",
    });
  };
  return (
    <LeftContentSection >
      <Fade direction='down' triggerOnce >
      <h6>{t(header)}</h6>
        <Row justify="space-between" align="middle" id={id}>
          <Col lg={11} md={11} sm={12} xs={24}>
            <SvgIcon src={icon} width="80%" height="80%" />
          </Col>
          <Col lg={11} md={11} sm={11} xs={24}>
            <ContentWrapper>
              <h6>{t(title)}</h6>
              <Content>{t(content)}</Content>
              <ServiceWrapper>
                <Row justify="space-between">
                  {typeof subHeader === "object" &&
                    subHeader.map((item: any, id: number) => {
                      return (
                        <Col key={id} span={11}>
                         {/**<SvgIcon src={item.icon} width="60px" height="60px" /> */} 
                          <MinTitle>{t(item.title)}</MinTitle>
                          <MinPara>{t(item.content)}</MinPara>
                          
                        </Col>
                      );
                    })}
                </Row>
              </ServiceWrapper>
              <ServiceWrapper>
                <Row justify="space-between">
                  {typeof section === "object" &&
                    section.map((item: any, id: number) => {
                      return (
                        <Col key={id} span={11}>
                          <SvgIcon src={item.icon} width="60px" height="60px" />
                          <MinTitle>{t(item.title)}</MinTitle>
                          <MinPara>{t(item.content)}</MinPara>
                          
                        </Col>
                      );
                    })}
                </Row>
              </ServiceWrapper>
            </ContentWrapper>
          </Col>
        </Row>
      </Fade>
    </LeftContentSection>
  );
};

export default withTranslation()(LeftContentBlock);
