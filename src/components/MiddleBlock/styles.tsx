import styled from "styled-components";

export const MiddleBlockSection = styled("section")`
  position: relative;
 // padding: 7.5rem 0 0rem;
  text-align: center;
 // display: flex;
  margin-top:12%;
  justify-content: center;
  @media screen and (max-width: 1024px) {
    padding: 2.5rem 0 1rem;
  }
  @media only screen and (max-width: 600px) {
    padding-top: 0rem;
    max-width: 100%
  }
  
`;

export const Content = styled("p")`
  padding: 0.75rem 0 0.75rem;

`;

export const ContentWrapper = styled("div")`


@media only screen and (max-width: 600px) {
  padding-top: 4rem;
  max-width: 100%
}
`;
