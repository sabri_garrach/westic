import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";
import { I18nextProvider } from "react-i18next";
import "antd/dist/antd.css";
import { useMediaQuery } from 'react-responsive'
import Router from "./router";
import i18n from "./translation";
import CSS from 'csstype';
import MediaQuery from 'react-responsive'

const h1Styles: CSS.Properties = {
  //backgroundImage=url(/img/svg/bg.svg),
  backgroundImage: "url(" + "/img/svg/bg.svg" + ")",
 // backgroundPosition: 'center',
  //backgroundSize: 'contain',
  backgroundRepeat: 'no-repeat',
  backgroundPositionY:'-53px',
  backgroundPositionX:'20px',
  //backgroundAttachment:'fixed'

};
const h2Styles: CSS.Properties = {
  //backgroundImage=url(/img/svg/bg.svg),
  backgroundImage: "url(" + "/img/svg/bg.svg" + ")",
 // backgroundPosition: 'center',
  //backgroundSize: 'contain',
  backgroundRepeat: 'no-repeat',
  backgroundPositionY:'-53px',
  backgroundPositionX:'0px',
  backgroundAttachment:'fixed'

};
const App = () => (<div>
      <div style={h1Styles}>
      <BrowserRouter >
    <I18nextProvider i18n={i18n}>
      <Router  />
    </I18nextProvider>
  </BrowserRouter>
  </div>
</div>
  
   

  
);

ReactDOM.render(<App />, document.getElementById("root"));
